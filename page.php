<?php theme_include('header'); ?>

		<section class="container">
			<h1><?php echo page_title(); ?></h1>

			<?php echo page_content(); ?>
		</section>

<?php theme_include('footer'); ?>